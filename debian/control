Source: r-cran-mvnfast
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-mvnfast
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-mvnfast.git
Homepage: https://cran.r-project.org/package=mvnfast
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               r-cran-rcpparmadillo,
               r-cran-bh
Testsuite: autopkgtest-pkg-r

Package: r-cran-mvnfast
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R fast multivariate normal and student's t methods
 Provides computationally efficient tools related to the multivariate
 normal and Student's t distributions. The main functionalities are:
 simulating multivariate random vectors, evaluating multivariate normal
 or Student's t densities and Mahalanobis distances. These tools are very
 efficient thanks to the use of C++ code and of the OpenMP API.
